On this page

[[_TOC_]]

# Pymp

A cryptocurrency pump and dump automated bot using Binance and Telegram APIs.

This is the automated version that automatically gets the announced altcoin from the Big Pump Signal Telegram channel. To use the manual version that requires entering the altcoin symbol manually, checkout the `manual` branch.

## **Disclaimer**:
This is **not a guarantee of profit**. This program aims to automate the process of participating in pump and dump cryptocurrency schemes that a large majority of manual participants lose money on! This program tries to gain a timing edge in order to buy as soon as possible at as low a price as possible. It is in no way a guarantee of making a profit and may well result in a loss due to the fluctuating nature of the pumps. Use at your own risk and **never put in more money than you are willing to lose.**

## 1. Prerequisites
* Have a Telegram account set up
* Join the https://t.me/bigpumpsignal Telegram channel
* Have a Binance account
* Have a BTC balance on Binance of above 5 euro (recommended)
* Set up Telegram API keys via:
    + Go to https://my.telegram.org
    + Enter your phone number and confirmation code.
    + Click on ‘API development tools’.
    + Copy your 'App api_id' and 'App api_hash' and save it to a trusted location.
    + Do not share these.
    
* Set up Binance API keys via:
    + Go to https://www.binance.com and log in.
    + Hover over account icon and click on 'API Management'.
    + Click on 'Create API' after providing a memorable name for the usecase.
    + Copy your 'API Key' (public) and 'Secret Key' (private) and save to a trusted location.<br />
    **Note:** Secret Key will disappear after leaving page and if not copied, a new API will need to be set up.
    + Do not share them.
    
## 2. Dependencies
* The following packages are required:
    * python3 (min 3.5, best latest)
    * pip3
    * python-binance (install via pip)
    * telethon (install via pip)
    * twisted (install via pip)
    
## 3. Usage
Run the program via the python3 command, example:
```bash
python3 pymp.py --quote_coin btc --amt 0.00012 --wait 20 --pct 500
```
The following command line arguments need to be specified:
* **--quote_coin**: the main coin that you will be buying the pumped altcoin with. In the bigpumpsignal pumps the main coin is BTC.
* **--amt**: the amount of main/quote coin to use to purchase as much altcoin as this amount can buy. Recommended to specify up to a max of 5 significant figures after decimal point (Do not specify more than 5 significant figures after the decimal point).
* **--wait**: number of seconds to wait before selling all the purchased altcoin unconditionally.
* **-pct**: the target percentage increase in the price of the altcoin at which to sell at, compared to the price at which it was bought.
**Note:** If you are using the program for the first time you will be prompted to:
  + Log into Telegram
  + Specify your Telegram keys
  + Specify your Binance keys
The keys will be saved to corresponding JSON files in the same folder as the program and a Telegram session file will also be created.<br />
**DO NOT SHARE THESE FILES WITH ANYONE!**<br />

A folder with the results of the run will be created.
    
## 4. Workflow
* The program first creates a timestamped folder for the results of the run.
* Then it will ask for first time users to enter their Telegram and Binance credentials (keys).

* The main work consists of waiting for the Telgram channel to announce the altcoin that will be pumped (channel uses BTC as the main coin).
* Once the altcoin is announced, the bot automatically places a spot market buy order for as much altcoin as the specified amount of main coin can buy (--amt arg).
* Then the bot monitors the price change of the altcoin as compared to the buy price for the period of time specified (--wait arg).
* If the price within that time increases by the target percentage increase (--pct arg), then the bot automatically places a spot market sell order to sell all the purchased altcoin for BTC.
* However, if the target percentage increase was not reached, the bot automatically places the same sell order when the waiting period elapses (--wait arg).
* The bot then continues to monitor the price of the altcoin for a total of 5 minutes overall and saves the pricehistory to a CSV file in the results folder so that it can be graphed in Excel later for postanalysis.
* This could allow users to better gauge when to sell and could improve usage of the bot on future pumps.
* The program exits automatically after it has finished monitoring the price.


