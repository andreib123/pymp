import os
import re
import sys
import csv
import json
import time
import math
import logging
import argparse
import threading
import multiprocessing
from ctypes import c_char_p

# python-binance lib
from binance.client import Client
from twisted.internet import reactor
from binance.websockets import BinanceSocketManager
from binance.exceptions import BinanceAPIException, BinanceOrderException

# telethon lib
from telethon import TelegramClient, events

# endpoints and keys for Binance and Telegram API setup
RES_PATH = ""  # path to created results directory
BINANCE_API_FILE = "binance-api.json"  # Binance API file for keys
BINANCE_API_KEY = "api-key"  # public key identifier
BINANCE_SECRET_KEY = "secret-key"  # private key identifier
TELEGRAM_API_FILE = "telegram-api.json"  # Telegram API file for keys
TELEGRAM_API_ID = 'api-id'  # public key identifier
TELEGRAM_API_HASH = 'api-hash'  # private key identifier
TELEGRAM_CHANNEL = 'https://t.me/bigpumpsignal'  # channel to read base coin from
MONITOR = 5 * 60 * 1000  # monitor the price of BASE_COIN/QUOTE_COIN for 5 minutes (in ms)
PCT_DEV = 5.0  # deviation in percentage increase

# set up global parameters
buy_price = 0  # initialise the buy price which will be set in buy function
cur_price = 0  # initialise most recent price returned by websocket to some value
sold = False
timer_transaction_t = None  # thread that will execute after specified wait period in order to sell
price_history = {}  # key=time, value=price

manager = multiprocessing.Manager()  # multiprocess variable sharing for telegram process to set base coin symbol
base_coin_shared = manager.Value(c_char_p, "")  # the base coin symbol
binance_client = None
telegram_client = None
telegram_p = None  # telegram base coin monitoring child process


def now():
    """
        :return: current time in milliseconds
    """
    return int(time.time() * 1000)


def formatted_time():
    """
        :return: formatted seconds epoch time as a 'H:M:S' string
    """
    return time.strftime('%H:%M:%S', time.gmtime(time.time()))


def mkdir_results(timestamp):
    path = "./results_" + timestamp + "/"
    if not os.path.exists(path):
        global RES_PATH
        os.mkdir(path)
        # logging.info(f"Directory {path} created")
        RES_PATH = path


def setup_logging():
    """
        set up logging to both console and logfile
    """
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)  # track INFO logging events (default was WARNING)
    root_logger.handlers = []  # clear handlers
    root_logger.addHandler(logging.StreamHandler(sys.stdout))  # handler to log to console
    root_logger.addHandler(logging.FileHandler(f"{RES_PATH}pump.log"))  # handler to log to file also
    root_logger.handlers[0].setFormatter(logging.Formatter("%(levelname)s: %(message)s"))  # log level and message
    root_logger.handlers[1].setFormatter(logging.Formatter("%(levelname)s: %(message)s"))


def save_price_history():
    """
        save price history to csv file
    """
    with open(f"{RES_PATH}{symbol}_pricelog.csv", 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(["time", "price"])
        for tick in price_history:
            csv_writer.writerow([tick, price_history[tick]])
    logging.info(f"Price monitoring stopped.")
    logging.info(f"{RES_PATH}{symbol}_pricelog.csv file created.")


def input_keys(args):
    """
        prompt user to enter their API public and private keys for the specified platform

        Parameters:
        args: (list):
        args[0]=client program, e.g. Binance, Telegram, Discord
        args[1]=public key client program specific name, e.g api-key, api-id
        args[2]=private key client program specific name, e.g. secret-key, api-hash

        Returns:
        str: public key
        str: private key
    """
    logging.info(f"{args[0]} API user keys either incomplete or not found.\nPlease insert your keys before usage.")
    pub = input(f"Enter {args[1]}:").strip()
    priv = input(f"Enter {args[2]}:").strip()

    return pub, priv


def get_keys(args):
    """
        Handle API keys load for any client

        Parameters:
        args: (list):
        args[0]=client program, e.g. Binance, Telegram, Discord
        args[1]=public key client program specific name, e.g api-key, api-id
        args[2]=private key client program specific name, e.g. secret-key, api-hash
        args[3]=client program specific JSON filename, e.g. binance-api.json, telegram-api.json

    """
    logging.info(f"Setting up {args[0]} client")
    if os.path.exists(args[3]):
        # Load key file
        with open(args[3], 'r') as f:
            keys = json.loads(f.read())

        # incomplete key file
        if args[1] not in keys or args[2] not in keys:
            # get new keys
            pub, priv = input_keys((args[0], args[1], args[2]))
            keys[args[1]] = pub
            keys[args[2]] = priv
            # update key file
            with open(args[3], 'w') as f:
                json.dump(keys, f, indent=4)
                logging.info(f"Generated '{args[3]}'")

    else:
        # Create blank keys
        keys = {
            args[1]: "",
            args[2]: ""
        }

        # get new keys
        pub, priv = input_keys((args[0], args[1], args[2]))
        keys[args[1]] = pub
        keys[args[2]] = priv
        # Insert into new file
        with open(args[3], 'w') as f:
            json.dump(keys, f, indent=4)
            logging.info(f"Generated '{args[3]}'")

    return keys[args[1]], keys[args[2]]


def setup_binance_client():
    global binance_client
    # create new Binance API client
    pub, priv = get_keys(("Binance", BINANCE_API_KEY, BINANCE_SECRET_KEY, BINANCE_API_FILE))
    try:
        binance_client = Client(pub, priv)
    except Exception as e:
        logging.error("BINANCE CLIENT CREATION FAILED!!!")
        logging.error(e)
        sys.exit(1)
    logging.info("Session initiated successfully with Binance API")

    # get QUOTE_COIN balance and assert there is enough in account
    quote_acc_amt = round(float(binance_client.get_asset_balance(asset=quote_coin)['free']), 8)
    assert quote_acc_amt >= quote_amt, f"ERROR: insufficient {quote_coin} funds " \
                                       f"({quote_acc_amt} < {quote_amt} specified by --amt)"


def price_update(msg):
    """
        callback function

        process incoming WebSocket price update for BASE_COIN/QUOTE_COIN trades:
            - calculate the percentage increase from the buy price
            - sell if percentage increase above args.pct
    """
    global cur_price

    new_price = float(msg['c'])  # current price
    price_history[formatted_time()] = new_price

    if not sold:
        if msg['e'] != 'error':

            if new_price == cur_price:
                return  # new trade had the same price as previous trade
            pct_increase = ((new_price - buy_price) / buy_price) * 100.0
            logging.info(f"Price percentage increase: {pct_increase}")
            if (pct_increase + PCT_DEV >= pct) or (pct_increase - PCT_DEV >= pct):
                # percentage increase reached
                sell("percentage increase reached")
                return
            cur_price = new_price
        else:
            logging.warning("Problem with price update: " + msg['e'])


def buy(symbol, quote_amt):
    """
           buy as much base coin that specified quote coin amount can buy

           :return: boolean True if buy order was filled
       """
    global buy_price

    pump_buy_t0 = now()  # time order got placed
    # Place spot buy order at market value
    try:
        buy_order = binance_client.create_order(symbol=symbol,
                                                side="BUY",
                                                type="MARKET",
                                                quoteOrderQty=quote_amt)
        with open(RES_PATH + "buy-order-response.json", 'w') as f:
            json.dump(buy_order, f, indent=4)
            logging.info(f"Generated {RES_PATH}buy-order-response.json")
    except (BinanceAPIException, BinanceOrderException) as e:
        logging.error("BUY FAILED!!!")
        logging.error(e)
        sys.exit(1)

    # real buy case
    if buy_order["status"] == "FILLED":
        pump_buy_t1 = buy_order["transactTime"]  # time order got filled
        pump_buy_ms = pump_buy_t1 - pump_buy_t0  # time taken to buy in ms
        s, ms = divmod(pump_buy_t1, 1000)
        fills = buy_order['fills']
        total_qty = sum([float(fill['qty']) for fill in fills])  # amount of base coin bought
        total_price = sum([float(fill['qty']) * float(fill['price']) for fill in fills])  # amount of quote coin paid
        buy_price = round(total_price / total_qty, 8)  # weighted average buy price in QUOTE_COIN for order
        logging.info("Bought {total_qty} {base} in {pump_buy_ms} ms for {total_price} {quote} @ {timestamp}.{ms:03d}"
                     .format(total_qty=total_qty, base=base_coin, pump_buy_ms=pump_buy_ms,
                             total_price=total_price, quote=quote_coin, buy_price=buy_price,
                             timestamp=time.strftime('%H:%M:%S', time.gmtime(s)), ms=ms))
        return True  # bought = True
    else:
        logging.error(f"Buy order has not been filled. Response:")
        logging.error(buy_order)
        sys.exit(1)


def sell(reason):
    """
        sell all base coins in wallet for quote coin

        set sold flag to true
        cancel the timer
    """
    global sold, timer_transaction_t
    if sold:
        return

    # ensure LOT_SIZE constraint passes
    step_size = float(symbol_info['filters'][2]['stepSize'])
    coin_amt = float(binance_client.get_asset_balance(asset=base_coin)['free'])
    coin_amt = float(math.floor(coin_amt * (1 / step_size))) / (1 / step_size)

    # time order got placed
    pump_sell_t0 = now()

    # Place spot sell order at market value
    try:
        sell_order = binance_client.create_order(symbol=symbol,
                                                 side="SELL",
                                                 type="MARKET",
                                                 quantity=coin_amt)
        with open(RES_PATH + "sell-order-response.json", 'w') as f:
            json.dump(sell_order, f, indent=4)
            logging.info(f"Generated {RES_PATH}sell-order-response.json")
    except (BinanceAPIException, BinanceOrderException) as e:
        logging.error("SELL FAILED!!!")
        logging.error(e)
        sys.exit(1)

    if sell_order["status"] == "FILLED":
        pump_sell_t1 = sell_order["transactTime"]  # time order got filled
        pump_sell_ms = pump_sell_t1 - pump_sell_t0  # time taken to sell in ms
        s, ms = divmod(pump_sell_t1, 1000)
        fills = sell_order['fills']
        total_qty = sum([float(fill['qty']) for fill in fills])  # amount of base coin sold
        total_price = sum([float(fill['qty']) * float(fill['price']) for fill in fills])  # amount of quote coin got
        sell_price = round(total_price / total_qty, 8)  # weighted average sell price of 1 BASE_COIN in QUOTE_COIN
        logging.info(
            "Sold {total_qty} {coin} in {pump_sell_ms} ms for {total_price} {quote} @ {ts}.{ms:03d} due to {reason}."
                .format(total_qty=total_qty, coin=base_coin, pump_sell_ms=pump_sell_ms, total_price=total_price,
                        quote=quote_coin, ts=time.strftime('%H:%M:%S', time.gmtime(s)), ms=ms, reason=reason))
        logging.info("Profit: {:04f}%".format(((sell_price - buy_price) / buy_price) * 100.0))
        sold = True
        if timer_transaction_t is not None:
            timer_transaction_t.cancel()
    else:
        logging.error(f"Sell order has not been filled. Response:")
        logging.error(sell_order)
        sys.exit(1)


def setup_telegram_client():
    global telegram_client
    # create new Telegram API client
    pub, priv = get_keys(("Telegram", TELEGRAM_API_ID, TELEGRAM_API_HASH, TELEGRAM_API_FILE))
    # create new client obj
    try:
        telegram_client = TelegramClient('pymp-telegram', int(pub), priv, auto_reconnect=False)
    except Exception as e:
        logging.error("TELEGRAM CLIENT CREATION FAILED!!!")
        logging.error(e)
        sys.exit(1)
    telegram_client.add_event_handler(msg_handler, events.NewMessage(chats=TELEGRAM_CHANNEL))
    telegram_client.start()
    logging.info("Session initiated successfully with Telegram API")


def wait_announcement_telegram():
    global telegram_p
    # wait for base coin announcement in telegram channel
    telegram_p = multiprocessing.Process(target=telegram_main)  # promoted to process to exit gracefully
    telegram_p.start()
    # telegram_process.join()
    while not base_coin_shared.value:
        time.sleep(0.01)  # sleep parent process for 10 ms

def telegram_main():
    """
        the main process for waiting on a base coin announcement in the telegram channel
    """
    # set up telegram client

    logging.info(f"Waiting for base coin announcement in the channel: {TELEGRAM_CHANNEL}")
    try:
        telegram_client.run_until_disconnected()  # infinite loop
    except Exception as e:
        sys.exit(0)


async def msg_handler(event):
    """
        callback function

        process incoming messages from specified telegram channel:
            - check if message is a base coin announcement
            - set base_coin global variable and exit process if it is
    """
    global base_coin_shared
    msg = event.message.message  # new message received in channel as string
    smb = re.search(r'(?<=/)[A-Z]{1,9}(?=_BTC)', msg)  # try to match base coin symbol (1 to 9 chars length)
    if smb is not None:
        base_coin_shared.value = smb.group(0).upper()  # extract the base coin symbol
        # sys.exit(0)  # terminate the telegram process


if __name__ == "__main__":
    # parse program arguments
    parser = argparse.ArgumentParser(
        description="Pump n dump automated buy/sell bot for 'BASE_COIN/QUOTE_COIN' market")
    parser.add_argument("--quote_coin", type=str, default=None, required=True,
                        help="quote coin to use for buy")
    parser.add_argument("--amt", type=str, default=None, required=True,
                        help="amount of quote coin to use for buy")
    parser.add_argument("--wait", type=int, default=0, required=True,
                        help="Time to wait between buy and sell, in seconds")
    parser.add_argument("--pct", type=str, default='10000.0', required=False,
                        help="Percentage increase from buy price at which to sell")
    args = parser.parse_args()

    # ensure wait arg is specified with a non zero value
    assert args.wait != 0, "ERROR: must specify a non zero value for --wait, use 'python pymp.py -h' for help"
    quote_coin = args.quote_coin.upper().strip()  # quote coin
    quote_amt = float(args.amt)  # amount of quote coin to use for buy
    pct = float(args.pct)  # target percentage increase in price of 1 BASE_COIN for QUOTE_COIN

    # make ./results_<timestamp>/ directory for results of the run
    init_time = time.strftime('%d_%m_%Y_%H-%M-%S', time.gmtime(time.time()))
    mkdir_results(init_time)

    # log to both console and logfile
    setup_logging()

    setup_binance_client()

    setup_telegram_client()

    # wait for base coin announcement in telegram channel
    wait_announcement_telegram()

    # base coin symbol received
    base_coin = base_coin_shared.value  # get symbol as string from shared value

    symbol = f"{base_coin}{quote_coin}"  # market exchange symbol
    symbol_info = binance_client.get_symbol_info(symbol)
    with open(RES_PATH + "symbol-info-response.json", 'w') as f:
        json.dump(symbol_info, f, indent=4)
        logging.info(f"Generated {RES_PATH}symbol-info-response.json")

    # bought successfully
    if buy(symbol, quote_amt):
        # initialise WebSocket for price polling
        bsm = BinanceSocketManager(binance_client)
        conn_key = bsm.start_symbol_ticker_socket(symbol, price_update)
        # start the WebSocket
        bsm.start()

        monitor_start = now()
        monitor_end = monitor_start + MONITOR

        # sell either when percentage increase is reached or time delay is reached, whichever comes first
        timer_transaction_t = threading.Timer(args.wait, sell, ["timer expiry"])  # function will run after wait time
        timer_transaction_t.start()
        timer_transaction_t.join()

        # keep monitoring price for MONITOR time
        logging.info(f"Continuing to monitor price for a total of {MONITOR / (1000 * 60)} minutes...")
        while now() < monitor_end:
            time.sleep(0.01)

        # stop websocket
        bsm.stop_socket(conn_key)
        reactor.stop()  # properly terminate WebSocket

        save_price_history()

        telegram_p.kill()  # kill the child telegram process
